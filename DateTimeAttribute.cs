using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace ResourcesDemo
{
public class DateTimeAttribute : ValidationAttribute
{
    public DateTimeAttribute() : base()
    {
        ErrorMessage = "{0} is an invalid";
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        DateTime dateTime;
        bool success = DateTime.TryParseExact(
                    (value as string).Trim(),
                    new string[] {
                        "yyyy'-'MM'-'dd' 'HH':'mm':'ss",
                        "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
                        "yyyy'-'MM'-'dd",
                    },
                    DateTimeFormatInfo.InvariantInfo,
                    DateTimeStyles.RoundtripKind,
                    out dateTime
                );

        if (!success)
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        return ValidationResult.Success;
    }
} 
}