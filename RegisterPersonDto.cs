using System.ComponentModel.DataAnnotations;

namespace ResourcesDemo
{
    public class RegisterPersonDto
    {
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [DateTime]
        public string BirthDate { get; set; }
    }    
}