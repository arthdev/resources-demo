using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace ResourcesDemo
{
public class DateTimeAttributeAdapter : AttributeAdapterBase<DateTimeAttribute>
{
    public DateTimeAttributeAdapter(DateTimeAttribute attribute, IStringLocalizer stringLocalizer) : base(attribute, stringLocalizer)
    {
    }

    public override void AddValidation(ClientModelValidationContext context) 
    {
    }

    public override string GetErrorMessage(ModelValidationContextBase validationContext)
    {
        var metadata = validationContext.ModelMetadata;
        return GetErrorMessage(metadata, metadata.GetDisplayName());
    }
}
}